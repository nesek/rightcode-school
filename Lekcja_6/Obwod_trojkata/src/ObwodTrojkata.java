public class ObwodTrojkata {

    public static boolean czyBokiZbudujaTrojkat(int a, int b, int c) {

        return !(a + b <= c || a + c <= b || b + c <= a);

    }

    public static int obwodTrojkata(int a, int b, int c)
            throws BokiNieStanowiaTrojkataException {

    if (!czyBokiZbudujaTrojkat(a, b, c))
        throw new BokiNieStanowiaTrojkataException();

        return a + b + c;
    }
}