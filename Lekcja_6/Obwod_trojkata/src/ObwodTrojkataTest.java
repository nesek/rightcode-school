import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

/**
 * Created by kliszews on 01/05/2017.
 */
public class ObwodTrojkataTest {
    @Test                                 // test 1
    public void czyPoliczyObwod() throws BokiNieStanowiaTrojkataException {
        int wynik = ObwodTrojkata.obwodTrojkata(2,3,4);
        Assertions.assertEquals(9, wynik);
    }

    @Test                    // test 2
    public void czyBladGdyBokiNieStanowiaTrojkata() {
        try {
            ObwodTrojkata.obwodTrojkata(1, 2, 3);
            // jezeli dojdzie do wykonania ponizszej linii, to znaczy,
            // ze wyjatek nie zostal rzucony
            Assertions.fail("Oczekiwany wyjatek nie zostal rzucony!");
        } catch (BokiNieStanowiaTrojkataException e) {
            // test przeszedl S- spodziewalismy sie takiego wyjatku
        }
    }

    @Test
    public void czyBokiZbudujaTrojkatTest() {
        Assertions.assertEquals(false, ObwodTrojkata.czyBokiZbudujaTrojkat(1,2,3));
    }

    @Test
    public void czyBokiZbudujaTrojkatTest2() {
        Assertions.assertEquals(true, ObwodTrojkata.czyBokiZbudujaTrojkat(3,2,3));

    }
}
