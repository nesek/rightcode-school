/**
 * Created by kliszews on 15/05/2017.
 */
public class ReverseOrderArgs {
    public static void main(String[] args) {
        for(int i = args.length-1; i >= 0; i--)
            System.out.println("Argument nr " + (i+1) + ": " + args[i]);

    }
}
