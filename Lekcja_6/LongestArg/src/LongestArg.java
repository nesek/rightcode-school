/**
 * Created by kliszews on 15/05/2017.
 */
public class LongestArg {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Nie podano zadnych ciagow. Zakonczenie programu.");
            System.exit(1);
        }
        int longest = 0;
        for (int i = 1; i < args.length; i++) {
            if (args[i].length() > args[longest].length())
                longest = i;
        }
        System.out.println("Longest argument is : " + args[longest]);
    }
}
