/**
 * Created by kliszews on 11/05/2017.
 */

public class DivideNumbers {

    public static int DivideNumbers(int a, int b)
            throws DivisionByZeroException {
        if (b == 0)
            throw new DivisionByZeroException();
        return a/b;
    }

    public static void test1() {
        try {
            assertEquals(DivideNumbers(8, 4), 2);
        } catch (DivisionByZeroException e) {
            System.out.println("Blad, dzielenie przez zero!");
        }
    }

    public static void testZero() {
        try {
            assertEquals(DivideNumbers(8, 0), 2);
        } catch (DivisionByZeroException e) {
            System.out.println("Blad, dzielenie przez zero!");
        }
    }

    public static void assertEquals(int expected, int actual) {
        if (expected != actual)
            System.out.println("Blad podczas dzielenia!");
    }


    public static void main(String[] args) {
        test1();
        testZero();
    }
}