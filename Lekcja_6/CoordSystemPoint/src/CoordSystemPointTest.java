import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CoordSystemPointTest {

    @Test
    public  void twoReferencesTest() {

        CoordSystemPoint a = new CoordSystemPoint(5, 5);
        CoordSystemPoint b = a;
        CoordSystemPoint c = b;

        Assertions.assertTrue(a.equals(b));
        Assertions.assertTrue(b.equals(a));
        Assertions.assertTrue(c.equals(a));
    }

    @Test
    public void twoSamePointsTest() {

        CoordSystemPoint a = new CoordSystemPoint(5,5);
        CoordSystemPoint b = new CoordSystemPoint(5,5);

        Assertions.assertTrue(a.equals(b));
    }

    @Test
    public void twoDifferentPointsTest() {

        CoordSystemPoint a = new CoordSystemPoint(5,5);
        CoordSystemPoint b = new CoordSystemPoint(5,10);

        Assertions.assertFalse(a.equals(b));
    }

    @Test
    public void valuesCheckTest() {

        CoordSystemPoint a = new CoordSystemPoint(5,10);

        Assertions.assertEquals(5, a.getX());
        Assertions.assertEquals(10, a.getY());
    }

    @Test
    public void valuesChangeTest() {

        CoordSystemPoint a = new CoordSystemPoint(5,10);
        a.setX(20);
        a.setY(30);

        Assertions.assertEquals(20, a.getX());
        Assertions.assertEquals(30, a.getY());
    }

    @Test
    public void oneDifferentObjectTypeTest() {

        CoordSystemPoint a = new CoordSystemPoint(5,5);
        String b = "Test";

        Assertions.assertFalse(a.equals(b));
    }
}

