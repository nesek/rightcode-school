/**
 * Created by kliszews on 15/05/2017.
 */
public class CoordSystemPoint {
    private int x, y;

    public CoordSystemPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean equals(Object o) {
        if (o == this ) return true;

        if (o == null || o.getClass() != this.getClass()) return false;

        CoordSystemPoint point2 = (CoordSystemPoint)o;

        if (this.x != point2.x || this.y != point2.y) return false;
        else return true;
    }

    public int getX () {
        return x;
    }

    public int getY () { return y; }

    public void setX (int x) {
        this.x = x;
    }

    public void setY (int y) {
        this.y = y;
    }

    public static void main(String[] args) {}
}
